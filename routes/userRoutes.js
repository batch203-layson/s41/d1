const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

router.post("/checkEmail", userControllers.checkEmailExists);



/* 
Route for user registration
*/
router.post("/register", userControllers.registerUser);

/* 
Route for user authentication
*/
router.post("/login", userControllers.loginUser);

/* 
Route for details of user
*/
router.get("/details", auth.verify, userControllers.getProfile);

/* 
Route for enrolling a user
*/
router.post("/enroll", auth.verify, userControllers.enroll);



module.exports = router;