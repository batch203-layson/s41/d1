const jwt = require("jsonwebtoken");

/* 
Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret key
*/
const secret = "CourseBookingApi";

/* 
[SECTION] 

JSON Web Tokens

Token Creation
Analogy:
    Pack the gift provided with a lock, which can only be open using the secret codew as the key
*/

// the "user" parameter 
module.exports.createAccessToken= (user) =>{
    console.log(user);
    const data ={
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {});
}

/* 
Generate a JSON web token using the jwt's "sign method"

syntax
jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])
*/


/* 
Token Verification

Analogy
Receive the gift and opent he lock to verify if the sender is legitimate and the gift was not tampered
*/

// middleware function
module.exports.verify = (req, res, next) => {
    // the token is retrieved from the request header
    let token = req.headers.authorization;
    //console.log(token);
    // If token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers
    if(token !== undefined)
    {
        // res.send({message: "Token received!"})
        // slice method to remove the bearer
        // res.send(token.slice())
        token=token.slice(7, token.length);
        // console.log(token);
        // res.send(token)
        
        /* 
        // validate the "token" using the "verify" method to decrypt the token using the secret method
        syntax
        jwt.verify(token, secretOrPrivateKey, [options/callBackFunction])
        */
        return jwt.verify(token, secret, (err, data) => {
            // if JWT is not valid
            if(err)
            {
                return res.send({auth: "Invalid token!"})
            }
            // if JWT is valid
            else
            {
                // Allows the application to proceed with the next middleware function/callback function in the route
                next();
            }
        })
    }
    else
    {
        res.send({message: "Auth failed. No token provided!"})
    }
}
/* 
// Token decryption
- Analogy
open the gift and get the content

 */

module.exports.decode=(token)=>{
    if(token !== undefined)
    {
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (err, data)=>{
            if(err)
            {
                return null;
            }
            else
            {
                /* 
                // decode method is used to obtain the information from the JWT 
                    syntax
                    jwt.decode(token, [options]);
                    returns an object with access to the "payload" property which contains the user information stored when the token is generated
                 */
                return jwt.decode(token, {complete:true}).payload;
            }
        })
    }
    else
    {
        // 
        return null
    }
}